var vp = document.getElementById("villaPlatzi");
var papel = vp.getContext("2d");

var teclas = {
    UP : 38,
    DOWN : 40,
    LEFT : 37,
    RIGHT : 39
};

    var xi = 250;
    var yi =250;
    
var fondo = 
{
	url: "tile.png",
	cargaOK : false
};
var vaca = 
{
	url: "vaca.png",
	cargaOK : false
};
var pollo = 
{
	url: "pollo.png",
	cargaOK : false
};
var cerdo = 
{
	url: "cerdo.png",
	cargaOK : false
};
//vamos a crear un objeto con js en vez de html

fondo.imagen = new Image(); //declaro una instancia "imagen" de la clase imagen 
fondo.imagen.src = fondo.url;         // igualo el atributo src a la fuente de la imagen
fondo.imagen.addEventListener("load", cargarFondo);


vaca.imagen = new Image();
vaca.imagen.src = vaca.url;
vaca.imagen.addEventListener("load", cargarVaca);

cerdo.imagen = new Image();
cerdo.imagen.src = cerdo.url;
cerdo.imagen.addEventListener("load", cargarCerdo);

pollo.imagen = new Image();
pollo.imagen.src = pollo.url;
pollo.imagen.addEventListener("load", cargarPollo);

document.addEventListener("keyup", dibujarTeclado);

function cargarFondo()
{
	fondo.cargaOK = true;
	dibujar();
}
function cargarVaca()
{
	vaca.cargaOK = true;
	dibujar();
}
function cargarCerdo()
{
	cerdo.cargaOK = true;
	dibujar();
}
function cargarPollo()
{
	pollo.cargaOK = true;
	dibujar();
}


function dibujarVaca()
{
	//le paso a la funcion de dibujar imagen la imagen y las coordenadas donde va poner el punto inicial
	var x = aleatorio(1,100);
	var y = aleatorio(1,100);
	papel.drawImage(vaca, x, y); 
}


function dibujar()
{
	var x = aleatorio(1,100);
	var y;
	var i=0;
	var cantidadAnimales;

	if (fondo.cargaOK == true)
	{
		//le paso a la funcion de dibujar imagen la imagen y las coordenadas donde va poner el punto inicial
		papel.drawImage(fondo.imagen, 0, 0);
	}

	if (vaca.cargaOK == true)
	{
		cantidadAnimales = aleatorio(0,10);
		for (i=0; i<=cantidadAnimales; i++)
		{
			y = aleatorio(90,410);
		    x = aleatorio(90,410);
		    //le paso a la funcion de dibujar imagen la imagen y las coordenadas donde va poner el punto inicial
		    papel.drawImage(vaca.imagen, x, y);
		}
	}

    if (pollo.cargaOK == true)
	{
		cantidadAnimales = aleatorio(0,10);
		for (i=0; i<=cantidadAnimales; i++)
		{
			y = aleatorio(90,410);
			x = aleatorio(90,410);
		    //le paso a la funcion de dibujar imagen la imagen y las coordenadas donde va poner el punto inicial
		    papel.drawImage(pollo.imagen, x, y);
		}
	}
	 
}

function aleatorio (min, max)
{
	var resultado;
	resultado = Math.floor(Math.random() *(max -min + 1)) + min;
	return resultado;
}

function dibujarTeclado (evento)
{
    console.log(evento);
    // var colorcito = "brown";
    var movimiento = 5;

    switch (evento.keyCode)
    {
        
        case teclas.DOWN:
        moverCerdo( xi, yi, xi, yi + movimiento);
        yi = yi + movimiento;
        break;

        case teclas.UP:
        moverCerdo(xi, yi, xi, yi - movimiento);
        yi = yi - movimiento;
        break;

        case teclas.LEFT:
        moverCerdo(xi, yi, xi - movimiento, yi);
        xi = xi - movimiento;
        break;

        case teclas.RIGHT:
        moverCerdo(xi, yi, xi + movimiento, yi);
        xi = xi + movimiento;
        break;
    }
}
    function moverCerdo( xi, yi, xf, yf )
    {
    	if (cerdo.cargaOK == true)
    	{
    		papel.drawImage(cerdo.imagen, xi, yi);
	    }
    }

document.addEventListener("mousedown", dibujarMouse);
var cuadrito = document.getElementById("area_De_Dibujo");
var papel = cuadrito.getContext("2d");
var x;
var y;

function dibujarLinea (color, xinicial, yinicial, xfinal, yfinal, lienzo)
{
    lienzo.beginPath();
    lienzo.strokeStyle = color;   
    lienzo.lineWidth = 3;
    lienzo.moveTo(xinicial, yinicial);
    lienzo.lineTo(xfinal, yfinal);
    lienzo.stroke();
    lienzo.closePath();
}

function dibujarMouse (evento)
{
    console.log(evento);
    dibujarLinea("black",x,y,evento.offsetX,evento.offsetY,papel);
    x = evento.layerX;
    y = evento.layerY;
}


//d va ser igual a canvas, ya que lo estoy igualando al id de la etiqueta canvas de mi html
var d = document.getElementById("dibujito"); 
//lienzo va ser el objeto mediante el cual voy a manejar mi canvas y defino si va ser 2d o 3d
var lienzo = d.getContext("2d");
console.log(lienzo);
//defino que se va hacer un trazo

dibujarLinea("green", 10, 10, 110, 10);
dibujarLinea("green", 110, 10, 110, 110);
dibujarLinea("green", 110, 110, 10, 110);
dibujarLinea("green", 10, 110, 10, 10);

function dibujarLinea (color, xinicial, yinicial, xfinal, yfinal)
{
	lienzo.beginPath();
    // Le doy color a la linea
    lienzo.strokeStyle = color;   
    //le digo hacia donde vaiiciar la linea
    lienzo.moveTo(xinicial, yinicial);
    //dibujo la linea 
    lienzo.lineTo(xfinal, yfinal);
    //le doy estilo a la linea (es importante ya que sin esto no se dibuja)
    lienzo.stroke();
    //cierro el trazo, si no lo hago la siguiente linea empieza desde el punto anterior
    lienzo.closePath();
}
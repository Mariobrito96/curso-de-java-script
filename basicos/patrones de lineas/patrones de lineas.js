var texto = document.getElementById("numero_lineas");
var button = document.getElementById("boton")
// ejecuto la funcion "dibujo_click" al hacer click en el boton
boton.addEventListener("click", dibujo_Click); 
//d va ser igual a canvas, ya que lo estoy igualando al id de la etiqueta canvas de mi html
var d = document.getElementById("dibujito"); 
var ancho = d.width;
// alert(ancho);
//lienzo va ser el objeto mediante el cual voy a manejar mi canvas y defino si va ser 2d o 3d
var lienzo = d.getContext("2d");
 dibujarLinea("black", 1, 1, 1, 300);
 dibujarLinea("black", 1, 299, 299, 299);

//defino que se va hacer un trazo

function dibujarLinea (color, xinicial, yinicial, xfinal, yfinal)
{
	lienzo.beginPath();
    // Le doy color a la linea
    lienzo.strokeStyle = color;   
    //le digo hacia donde vaiiciar la linea
    lienzo.moveTo(xinicial, yinicial);
    //dibujo la linea 
    lienzo.lineTo(xfinal, yfinal);
    //le doy estilo a la linea (es importante ya que sin esto no se dibuja)
    lienzo.stroke();
    //cierro el trazo, si no lo hago la siguiente linea empieza desde el punto anterior
    lienzo.closePath();
}

function dibujo_Click ()
{
    var l = 0;
    var yinicial;
    var xfinal;
    var xinicial;
    var yfinal;
    var x = parseInt(texto.value);
    var espacio = ancho/x;
    

    for (l=0; l<x; l++)
    {
        yinicial = espacio * l;
        xfinal = espacio *(l+1);
        dibujarLinea("red", 0, yinicial, xfinal, 300);
        console.log(l); 
    }

    for (l=0; l<x; l++)
    {
     xinicial = espacio * l;
     yfinal = espacio *(l+1);
     dibujarLinea("green", xinicial, 0, 300, yfinal);
     console.log(l);
    }

}